'use strict';

$('.js-menu-toggle').click(function(e) {
	e.preventDefault();
	$('body').toggleClass('navigation-open');
});

/* map begin*/
function mapsView(pos1, pos2) {
	var center = { lat: pos1, lng: pos2 };
	var map = new google.maps.Map(document.getElementById('js-map'), {
		zoom: 10,
		center: center,
	});
	var marker = new google.maps.Marker({ position: center, map: map });
}
if ($('div').is('.js-map')) {
	let position = $('.js-map').data('position');
	let arrPosition = position.split(', ');
	let position1 = Number(arrPosition[0]);
	let position2 = Number(arrPosition[1]);
	mapsView(position1, position2);
}
/* map end*/

$('.js-slider').slick({
	centerMode: true,
	centerPadding: '200px',
	slidesToShow: 1,
	focusOnSelect: true,
	responsive: [
		{
			breakpoint: 950,
			settings: {
				centerMode: true,
				centerPadding: '150px',
				slidesToShow: 1,
				focusOnSelect: true,
			},
		},
		{
			breakpoint: 600,
			settings: {
				centerMode: true,
				centerPadding: '100px',
				slidesToShow: 1,
				focusOnSelect: true,
			},
		},
		{
			breakpoint: 480,
			settings: {
				centerMode: true,
				centerPadding: '50px',
				slidesToShow: 1,
				focusOnSelect: true,
			},
		},
	],
});

if ($('.main').hasClass('index-main')) {
	$('body').addClass('index-page');
}

// скрытие списка маршрутов
function showRoutes(list) {
	let displayView;
	if (!$(list).hasClass('show')) {
		displayView = 'none';
		$(list).addClass('show');
	} else {
		displayView = 'block';
		$(list).removeClass('show');
	}
	if ($(list).find('li').length <= 23) {
		$(list).next('.route-list__button').hide();
	}

	$(list).find('li').each(function(i, element) {
		if (i > 23) {
			$(element).css({ display: displayView });
		}
	});
}

showRoutes($('.route-list'));

$('.route-list__button').click(function() {
	var routeList = $(this).prev('.route-list');
	showRoutes($(routeList));
});

function fastMapCheck(params) {
	$('.fast-map__label').removeClass('active-prev');
	$('.fast-map__label').removeClass('active-next');

	$('.fast-map__label').each(function() {
		if ($(this).hasClass('active')) {
			$(this).prev().addClass('active-prev');
			$(this).next().addClass('active-next');
		}
	});
}
fastMapCheck();

$('.fast-map input').change(function() {
	if (this.checked) {
		var parentLine = $(this).closest('.fast-map__line');
		var oldActive = $(parentLine).find('.active');
		$(oldActive).removeClass('active');

		$(this).parent().addClass('active');
	}
	fastMapCheck();
});

$('.js-object-slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: true,
	asNavFor: '.js-object-slider-min',
});

$('.js-object-slider-min').slick({
	slidesToShow: 5,
	slidesToScroll: 1,
	asNavFor: '.js-object-slider',
	dots: false,
	centerMode: false,
	focusOnSelect: true,
});

$('.gallery__item').magnificPopup({ type: 'image', gallery: { enabled: true } });

$('.open-popup-link').magnificPopup({
	type: 'inline',
	midClick: true,
});

//при изменении input описание фиксируется вверху
$('.form-group input').change(function() {
	if ($(this).val() != '') {
		$(this).parent().find('label').addClass('active');
	} else {
		$(this).parent().find('label').removeClass('active');
	}
});
$('.form-group input')
	.focusin(function() {
		$(this).parent().find('label').addClass('active');
	})
	.focusout(function() {
		if ($(this).val() != '') {
			$(this).parent().find('label').addClass('active');
		} else {
			$(this).parent().find('label').removeClass('active');
		}
	});
$('.form-group textarea')
	.focusin(function() {
		$(this).parent().find('label').addClass('active');
	})
	.focusout(function() {
		if ($(this).val() != '') {
			$(this).parent().find('label').addClass('active');
		} else {
			$(this).parent().find('label').removeClass('active');
		}
	});

$('.form-group textarea').on('change', function() {
	if ($(this).val() != '') {
		$(this).parent().find('label').addClass('active');
	} else {
		$(this).parent().find('label').removeClass('active');
	}
});

$('.has-child>a').click(function(e) {
	e.preventDefault();
	$(this).next().toggleClass('open');
});

$('.js-slide-content').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	responsive: [
		{
			breakpoint: 950,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
			},
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			},
		},
	],
});

if ($('div').is('.slider__item')) {
	$('.slider__item img').each(function(i, element) {
		var src = $(element).attr('src');
		RGBaster.colors(src, {
			exclude: ['rgb(255,255,255)'],
			success: function(payload) {
				$(element).parent().css({ background: payload.dominant });
			},
		});
	});
}

$('.fast-map__wrap').each(function(i, item) {
	var element = $(item).find('.fast-map__line');

	if (element[0].scrollWidth > element[0].offsetWidth) {
		$(item).find('.fast-map__arrow').show();
	}
});

function fastMapArrow() {
	$('.fast-map__arrow-right').click(function() {
		var length = $(this).parent().find('.fast-map__label').width();
		var line = $(this).parent().find('.fast-map__line');
		var position = line.position().left;
		$(line).css({ transform: 'translateX(' + (position - length) + 'px)' });
		fastMapArrowCheck($(line), $(this).prev(), $(this));
	});
	$('.fast-map__arrow-left').click(function() {
		var length = $(this).parent().find('.fast-map__label').width();
		var line = $(this).parent().find('.fast-map__line');
		var position = line.position().left;
		$(line).css({ transform: 'translateX(' + (position + length) + 'px)' });
		fastMapArrowCheck($(line), $(this), $(this).next());
	});
}

function fastMapArrowCheck(line, left, right) {
	var line = $(line);
	if (line.position().left >= 0) {
		$(left).attr('disabled', 'disabled');
	} else {
		$(left).removeAttr('disabled');
	}

	if (line[0].scrollWidth + line.position().left <= line[0].offsetWidth) {
		$(right).attr('disabled', 'disabled');
	} else {
		$(right).removeAttr('disabled');
	}
}

fastMapArrow();

// (function($) {
// 	/* map route begin*/

// 	var mapRouteWidth = $('#main-route-map').width();
// 	$('#main-route-map').height(mapRouteWidth * 0.61);

// 	function initMap() {
// 		var map = new google.maps.Map(document.getElementById('main-route-map'), {
// 			zoom: 6,
// 			center: { lat: 53.907332, lng: 27.820782 },
// 		});

// 		var markers = locations.map(function(location, i) {
// 			return new google
// 				.maps.Marker({ position: location[0], label: { position: 'relative', color: '#000', class: 'labels-we', text: location[1], fontWeight: 'bold', fontSize: '15px' } });
// 		});

// 		var mcOptions = {
// 			// imagePath: 'https://googlemaps.github.io/js-marker-clusterer/images/m',
// 			styles: [
// 				{
// 					url: '//maxmanchak.com/test/valatsuga/m1.png',
// 					width: 46,
// 					height: 79,
// 					textSize: 30,
// 					textColor: 'black',
// 					backgroundPosition: 'bottom',
// 					anchorText: [-40, 0],
// 				},
// 			],
// 		};
// 		var markerCluster = new MarkerClusterer(map, markers, mcOptions);
// 	}

// 	var locations = [
// 		[{ lat: 53.907332, lng: 25.820782 }, 'название 1'],
// 		[{ lat: 54.907332, lng: 26.820782 }, 'название 2'],
// 		[{ lat: 53.107332, lng: 23.920782 }, 'Название 3'],
// 		[{ lat: 53.207332, lng: 24.820782 }, 'Название 4'],
// 		[{ lat: 55.307332, lng: 28.820782 }, 'Название 5'],
// 		[{ lat: 53.407332, lng: 27.520782 }, 'Название 6'],
// 		[{ lat: 53.507332, lng: 27.120782 }, 'Название 7'],
// 		[{ lat: 53.607332, lng: 25.220782 }, 'Название 8'],
// 		[{ lat: 54.707332, lng: 26.320782 }, 'Название 9'],
// 		[{ lat: 53.807332, lng: 27.420782 }, 'Название 10'],
// 		[{ lat: 53.907332, lng: 28.620782 }, 'Название 11'],
// 		[{ lat: 52.107332, lng: 27.520782 }, 'Название 12'],
// 		[{ lat: 52.207332, lng: 25.720782 }, 'Название 13'],
// 		[{ lat: 52.307332, lng: 26.820782 }, 'Название 14'],
// 		[{ lat: 52.407332, lng: 27.220782 }, 'Название 15'],
// 		[{ lat: 55.507332, lng: 28.320782 }, 'Название 16'],
// 		[{ lat: 52.607332, lng: 27.320782 }, 'Название 17'],
// 		[{ lat: 52.707332, lng: 24.420782 }, 'Название 18'],
// 		[{ lat: 52.807332, lng: 25.820782 }, 'Название 19'],
// 		[{ lat: 52.907332, lng: 26.20782 }, 'Название 20'],
// 		[{ lat: 52.907332, lng: 27.720782 }, 'Название 21'],
// 		[{ lat: 53.907332, lng: 28.820782 }, 'Название 22'],
// 	];

// 	initMap();
// 	/* map route end*/
// })(jQuery);
